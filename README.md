**READ ME**

START-MINING
1. Go to minesolo.com and download the blockchain snapshot
2. Copy paste blockchain.raw in the folder with solod.exe
3. Run Import-bcsnapshot batch file
4. Run solod.exe to complete syncing (Do not close solod.exe after syncing)
5. Run wallet-cli.exe, Create your wallet and enter your new password
6. Extract wallet address and replace "YOUR_MINING_ADDRESS" with your wallet 
   address in Start-Mining batch file
7. Run Start-Mining batch file
8. Happy Mining!!

In the event of a corrupted blockchain
1. Go to C:\ProgramData and delete solo folder
2. Go to minesolo.com and download the blockchain snapshot
2. Copy paste blockchain.raw in the folder with solod.exe
3. Run Import-bcsnapshot

--OR--

1. Run Pop-Blocks.bat
2. Run Start-Mining.bat